<?php

use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('smigiela_articles')->insert([
            'title' => 'Windows - podstawy',
            'description' => 'Windows - podstawy - opis',
            'slug' => 'windows-podstawy',
            'category_id' => '1',
            'content' => 'Kategoria poświęcona systemowi Windows',
            'published_at' => '2020-04-16 18:39:34',
        ]);
        DB::table('smigiela_articles')->insert([
            'title' => 'Linux - jak naprawić gruba?',
            'description' => 'Linux - podstawy - opis',
            'slug' => 'linux-jak-naprawic-gruba',
            'category_id' => '1',
            'content' => 'Kategoria poświęcona systemowi Linux',
            'published_at' => '2020-03-16 18:39:34',
        ]);
        DB::table('smigiela_articles')->insert([
            'title' => 'Bezpieczeństwo aplikacji mobilnych',
            'description' => 'Bezpieczeństwo - podstawy - opis',
            'slug' => 'bezpieczenstwo-aplikacji-mobilnych',
            'category_id' => '2',
            'content' => 'Kategoria poświęcona zabezpieczeniom',
            'published_at' => '2020-05-16 18:39:34',
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('smigiela_categories')->insert([
            'title' => 'Windows',
            'slug' => 'windows',
            'description' => 'Kategoria poświęcona systemowi Windows',
        ]);
        DB::table('smigiela_categories')->insert([
            'title' => 'Linux',
            'slug' => 'linux',
            'description' => 'Kategoria poświęcona systemowi Linux',
        ]);
        DB::table('smigiela_categories')->insert([
            'title' => 'Bezpieczeństwo',
            'slug' => 'bezpieczenstwo',
            'description' => 'Kategoria poświęcona zabezpieczeniom',
        ]);
    }
}

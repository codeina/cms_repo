<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ArticlesMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smigiela_articles', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->string('description', 255);
            $table->string('slug')->nullable()->unique();
            $table->unsignedBigInteger('category_id');
            $table->longText('content');
            $table->softDeletes('deleted_at')->nullable();
            $table->date('published_at')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('smigiela_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smigiela_articles');
    }
}

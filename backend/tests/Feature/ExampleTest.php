<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * Check correct visit main page.
     * @test
     * @return void
     */
    public function show_main_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}

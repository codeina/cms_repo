<?php

namespace Tests\Feature;

use App\Models\Category;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test checking function of store category in database and read this from DB.
     * @test
     * @return void
     */
    public function store_and_show_the_category()
    {
        // Arrange
        // Dodajmy do bazy danych wpis
        $category = Category::create([
            'title' => 'Hacking',
            'slug' => 'Hack-ing',
            'description' => 'Hacking is description'
        ]);

        // Act
        // Wykonajmy zapytanie pod adres wpisu
        $response = $this->get('/categories/' . $category->id);

        // Assert
        // Sprawdźmy że w odpowiedzi znajduje się tytuł wpisu
        $response->assertStatus(200)
            ->assertSeeText('Hacking');
    }

    /**
     * Check delete record from db
     * @test
     */
//    public function delete_category_from_database()
//    {
//        // Arrange
//        // Dodajmy do bazy danych wpis
//        $category = Category::create([
//            'title' => 'master',
//            'slug' => 'mast-er',
//            'description' => 'Hacking is description'
//        ]);
//
//        // Act
//
//        // Assert
//    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => 'auth', 'prefix' => 'blog'], function () {
    Route::get('/categories', 'Admin\CategoriesController@index')->name('categories.index');
    Route::get('/categories/create', 'Admin\CategoriesController@create')->name('categories.create');
    Route::get('/categories/{id}', 'Admin\CategoriesController@show')->name('categories.show');
    Route::get('/categories/{id}/edit', 'Admin\CategoriesController@edit')->name('categories.edit');
    Route::post('/categories', 'Admin\CategoriesController@store')->name('categories.store');
    Route::patch('/categories/{id}', 'Admin\CategoriesController@update')->name('categories.update');
    Route::delete('/categories/{id}', 'Admin\CategoriesController@delete')->name('categories.delete');

    Route::get('/articles', 'Admin\ArticlesController@index')->name('articles.index');
    Route::get('/articles/create', 'Admin\ArticlesController@create')->name('articles.create');
    Route::get('/articles/{id}', 'Admin\ArticlesController@show')->name('articles.show');
    Route::get('/articles/{id}/edit', 'Admin\ArticlesController@edit')->name('articles.edit');
    Route::post('/articles', 'Admin\ArticlesController@store')->name('articles.store');
    Route::patch('/articles/{id}', 'Admin\ArticlesController@update')->name('articles.update');
    Route::delete('/articles/{id}', 'Admin\ArticlesController@delete')->name('articles.delete');

    Route::get('/tags', 'Admin\TagsController@index')->name('tags.index');
    Route::get('/tags/create', 'Admin\TagsController@create')->name('tags.create');
    Route::get('/tags/{id}', 'Admin\TagsController@show')->name('tags.show');
    Route::get('/tags/{id}/edit', 'Admin\TagsController@edit')->name('tags.edit');
    Route::post('/tags', 'Admin\TagsController@store')->name('tags.store');
    Route::patch('/tags/{id}', 'Admin\TagsController@update')->name('tags.update');
    Route::delete('/tags/{id}', 'Admin\TagsController@delete')->name('tags.delete');
});




Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');

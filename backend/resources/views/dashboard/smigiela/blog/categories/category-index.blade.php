@extends('layouts/admin', ['pageSlug' => 'categories', 'sectionName' => 'Blog',  'pageName' => 'Kategorie - lista'])
@section('content')
    @include('dashboard.components.message')
    <div class="card">
    {{--        <div class="card-header">--}}
    {{--            <h3 class="card-title">DataTable with default features</h3>--}}
    {{--        </div>--}}
    <!-- /.card-header -->
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
{{--                        <div id="example1_filter" class="dataTables_filter"><label>Search:<input type="search"--}}
{{--                                                                                                 class="form-control form-control-sm"--}}
{{--                                                                                                 placeholder=""--}}
{{--                                                                                                 aria-controls="example1"></label>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending">Nazwa
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        aria-label="Browser: activate to sort column ascending">Opis
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="Platform(s): activate to sort column ascending">Artykuły
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="Engine version: activate to sort column ascending">Akcje
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr role="row" class="odd">
                                    <td tabindex="0" class="sorting_1">{{$category->title}}</td>
                                    <td>{{$category->description}}</td>
                                    <td>@if($category->articles->count() == '0')
                                        -
                                        @else
                                            {{$category->articles->count()}}</td>
                                    @endif
                                    <td>
                                        <div class="row">
                                            <div class="col-xs-4 pl-2"><a href="{{route('categories.show', $category->id)}}" class="btn btn-sm btn-info">Pokaż</a></div>
                                            <div class="col-xs-4 pl-2"><a href="{{route('categories.edit', $category->id)}}" class="btn btn-sm btn-primary">Edytuj</a></div>
                                            <div class="col-xs-4 pl-2">
                                                <form action="{{ route('categories.delete', $category->id) }}" method="post">
                                                    <input type="hidden" name="_method" value="delete" />
                                                    {{csrf_field()}}
                                                    <button class="btn btn-sm btn-danger">{{ __('Usuń') }}</button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
{{--                        <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10--}}
{{--                            of 57 entries--}}
{{--                        </div>--}}

                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                            {{$categories->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

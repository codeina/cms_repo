@extends('layouts/admin', ['pageSlug' => 'categories', 'sectionName' => 'Blog',  'pageName' => 'Kategorie - szczegóły'])
@section('content')
<div class="card">

    <!-- /.card-header -->
    <div class="card-body text-left">
        <div class="text-left">
            <h4>Nazwa: <span>{!! $category->title !!}</span></h4>
            <h4>Slug: <span>{!! $category->slug !!}</span></h4>
            <h4>Opis: <span>{!! $category->description !!}</span></h4>
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection

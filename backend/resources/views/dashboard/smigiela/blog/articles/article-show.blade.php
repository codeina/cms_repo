@extends('layouts/admin', ['pageSlug' => 'articles', 'sectionName' => 'Blog',  'pageName' => 'Artykuł - szczegóły'])
@section('content')

    @include('dashboard.components.message')
<div class="card card-widget">
    <div class="card-header">
        <div class="user-block">
            <img class="img-circle" src="{{asset('bower_components/admin-lte/dist')}}/img/avatar.png"" alt="User Image">
            <span class="username"><a href="#">Daniel Śmigiela</a></span>
            <span class="description">Opublikowano: {!! date('d-m-Y', strtotime($article->published_at)) !!} | Kategoria: {!! $article->category->title !!}</span>
        </div>
        <!-- /.user-block -->
{{--        <div class="card-tools">--}}
{{--            <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">--}}
{{--                <i class="far fa-circle"></i></button>--}}
{{--            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>--}}
{{--            </button>--}}
{{--            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>--}}
{{--            </button>--}}
{{--        </div>--}}
{{--        <!-- /.card-tools -->--}}
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <!-- post text -->
        <strong>{!! $article->description !!}</strong>

        <div>{!! $article->content !!}</div>

        @foreach ($article->tags as $tag)
            <p>{{$tag->name}}</p>
        @endforeach

{{--        <!-- Attachment -->--}}
{{--        <div class="attachment-block clearfix">--}}
{{--            <img class="attachment-img" src="{{asset('bower_components/admin-lte/dist')}}/img/avatar.png" alt="Attachment Image">--}}

{{--            <div class="attachment-pushed">--}}
{{--                <h4 class="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4>--}}

{{--                <div class="attachment-text">--}}
{{--                    Description about the attachment can be placed here.--}}
{{--                    Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a>--}}
{{--                </div>--}}
{{--                <!-- /.attachment-text -->--}}
{{--            </div>--}}
{{--            <!-- /.attachment-pushed -->--}}
{{--        </div>--}}
{{--        <!-- /.attachment-block -->--}}

        <!-- Social sharing buttons -->
{{--        <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i> Share</button>--}}
{{--        <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>--}}
{{--        <span class="float-right text-muted">45 likes - 2 comments</span>--}}
    </div>
    <!-- /.card-body -->
    <div class="card-footer card-comments">
        <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('bower_components/admin-lte/dist')}}/img/avatar2.png" alt="User Image">

            <div class="comment-text">
                    <span class="username">
                      Maria Gonzales
                      <span class="text-muted float-right">8:03 PM Today</span>
                    </span><!-- /.username -->
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
            </div>
            <!-- /.comment-text -->
        </div>
        <!-- /.card-comment -->
        <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('bower_components/admin-lte/dist')}}/img/avatar3.png">
            <div class="comment-text">
                    <span class="username">
                      Nora Havisham
                      <span class="text-muted float-right">8:03 PM Today</span>
                    </span><!-- /.username -->
                The point of using Lorem Ipsum is that it hrs a morer-less
                normal distribution of letters, as opposed to using
                'Content here, content here', making it look like readable English.
            </div>
            <!-- /.comment-text -->
        </div>
        <!-- /.card-comment -->
    </div>
    <!-- /.card-footer -->
    <div class="card-footer">
        <form action="#" method="post">
            <img class="img-fluid img-circle img-sm" src="{{asset('bower_components/admin-lte/dist')}}/img/user2-160x160.jpg" alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
                <input type="text" class="form-control form-control-sm" placeholder="Skomentuj - enter wysyła komentarz">
            </div>
        </form>
    </div>
    <!-- /.card-footer -->
    <div></div></div>
@endsection

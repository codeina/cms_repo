@extends('layouts/admin', ['pageSlug' => 'tags', 'sectionName' => 'Blog',  'pageName' => 'Tag - szczegóły'])
@section('content')
<div class="card">

    <!-- /.card-header -->
    <div class="card-body text-left">
        <div class="text-left">
            <h4>Nazwa: <span>{!! $tag->name !!}</span></h4>
            @foreach($tag->articles as $art)
                {!! $art->title !!}
                @endforeach
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection

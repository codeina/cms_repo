<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Atrybut :attribute musi być zaakceptowany.',
    'active_url' => 'W polu :attribute nie ma poprawnego URL',
    'after' => 'W polu :attribute należy wpisać datę po dacie: :date.',
    'after_or_equal' => 'W polu :attribute należy wpisać datę po lub równą z: :date.',
    'alpha' => 'W polu :attribute można używać tylko liter.',
    'alpha_dash' => 'W polu :attribute można używać tylko liter, cyfr, myślników i podkreśleń.',
    'alpha_num' => 'W polu :attribute można używać tylko liter i cyfr.',
    'array' => 'Atrybut :attribute musi być tablicą.',
    'before' => 'W polu :attribute należy podać datę wcześniejszą niż: :date.',
    'before_or_equal' => 'W polu :attribute należy podać datę wcześniejszą lub równą z: :date.',
    'between' => [
        'numeric' => 'W polu :attribute wartość musi być pomiędzy :min and :max.',
        'file' => 'Plik :attribute musi być pomiędzy :min and :max kilobajtów.',
        'string' => 'Tekst w polu :attribute musi zawierać pomiędzy :min and :max znaków.',
        'array' => 'Tablica :attribute musi zawierać pomiędzy :min and :max elementów.',
    ],
    'boolean' => 'Pole :attribute musi zawierać prawda/fałsz.',
    'confirmed' => 'W polu :attribute potwierdzenie nie pasuje.',
    'date' => 'W polu :attribute nie podano prawidłowej daty.',
    'date_equals' => 'W polu :attribute należy podać datę równą z: :date.',
    'date_format' => 'W polu :attribute użyj poprawnego formatu: :format.',
    'different' => 'Pola :attribute oraz :other muszą być różne.',
    'digits' => 'W polu :attribute muszą być :digits liczby.',
    'digits_between' => 'W polu :attribute musi być pomiędzy :min i :max liczb.',
    'dimensions' => 'Zdjęcie :attribute ma niepoprawne wymiary.',
    'distinct' => 'Pole :attribute ma zduplikowaną wartość.',
    'email' => 'W polu :attributenależy podać poprawny adres e-mail.',
    'ends_with' => 'Pole :attribute musi być zakończone: :values.',
    'exists' => 'Wybrane pole :attribute jest niepoprawne.',
    'file' => 'W polu :attribute należy wskazać pliks.',
    'filled' => 'Pole :attribute musi mieć watość.',
    'gt' => [
        'numeric' => 'W polu :attribute należy podać wartość większą niż: :value.',
        'file' => 'Plik w polu :attribute musi być większy niż :value kilobajtów.',
        'string' => 'Napis :attribute musi byc większy niż :value znaków.',
        'array' => 'Tablica :attribute musi zawierać więcej niż :value obiektów.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];

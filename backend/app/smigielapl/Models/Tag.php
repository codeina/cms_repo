<?php

namespace App\smigielapl\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;
    protected $table = 'smigiela_tags';

    protected $fillable = ['name'];

    public function taggable()
    {
        return $this->morphTo();
    }

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'taggable');
    }
}

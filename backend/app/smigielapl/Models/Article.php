<?php

namespace App\smigielapl\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $table = 'smigiela_articles';

    protected $fillable = ['title', 'description', 'slug', 'category_id', 'content', 'published_at'];

    protected $casts = [
        'created_at'   => 'datetime:Y-m-d H:i',
        'updated_at'   => 'datetime:Y-m-d H:i',
        'deleted_at'   => 'datetime:Y-m-d H:i',
        'published_at' => 'datetime:Y-m-d'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->morphMany(Tag::class, 'taggable');
    }

}

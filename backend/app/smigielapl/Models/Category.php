<?php

namespace App\smigielapl\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $table = 'smigiela_categories';

    protected $fillable = ['title', 'slug', 'description'];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}

<?php


namespace App\smigielapl\Repositories\Eloquent;


use App\Repositories\Eloquent\BaseRepository;
use App\smigielapl\Models\Category;
use App\smigielapl\Repositories\CategoriesRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CategoriesRepository extends BaseRepository implements CategoriesRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;
    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

}

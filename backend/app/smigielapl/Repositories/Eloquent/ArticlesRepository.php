<?php


namespace App\smigielapl\Repositories\Eloquent;


use App\Repositories\Eloquent\BaseRepository;
use App\smigielapl\Models\Article;
use App\smigielapl\Repositories\ArticlesRepositoryInterface;

class ArticlesRepository extends BaseRepository implements ArticlesRepositoryInterface
{
    protected $model;

    public function __construct(Article $model)
    {
        parent::__construct($model);
    }

}

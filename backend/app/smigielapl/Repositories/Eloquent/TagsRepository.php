<?php


namespace App\smigielapl\Repositories\Eloquent;


use App\Repositories\Eloquent\BaseRepository;
use App\smigielapl\Models\Tag;
use App\smigielapl\Repositories\TagsRepositoryInterface;

class TagsRepository extends BaseRepository implements TagsRepositoryInterface
{
    protected $model;

    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }
}

<?php

namespace App\Providers;

use App\smigielapl\Repositories\ArticlesRepositoryInterface;
use App\smigielapl\Repositories\CategoriesRepositoryInterface;
use App\smigielapl\Repositories\Eloquent\ArticlesRepository;
use App\smigielapl\Repositories\Eloquent\CategoriesRepository;
use App\smigielapl\Repositories\Eloquent\TagsRepository;
use App\smigielapl\Repositories\TagsRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CategoriesRepositoryInterface::class, CategoriesRepository::class);
        $this->app->bind(ArticlesRepositoryInterface::class, ArticlesRepository::class);
        $this->app->bind(TagsRepositoryInterface::class, TagsRepository::class);
    }
}

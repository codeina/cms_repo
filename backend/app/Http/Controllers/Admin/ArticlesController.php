<?php

namespace App\Http\Controllers\Admin;

use App\smigielapl\Models\Category;
use App\smigielapl\Models\Tag;
use App\smigielapl\Repositories\ArticlesRepositoryInterface;
use App\smigielapl\Validators\ArticleStore;
use App\smigielapl\Validators\ArticleUpdate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    protected  $repository;

    public function __construct(ArticlesRepositoryInterface $repository)
    {
        return $this->repository = $repository;
    }

    public function index()
    {
        $articles = $this->repository->getAll(10);
        return view('dashboard.smigiela.blog.articles.article-index', compact('articles'));
    }

    public function show($id)
    {
        $article = $this->repository->findById($id);
        return view('dashboard.smigiela.blog.articles.article-show', compact('article'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('dashboard.smigiela.blog.articles.article-create', compact('categories'));
    }

    public function store(ArticleStore $request)
    {
        $article = $this->repository->create($request->validated());
        $tag = new Tag;
        $tag->name = $request->tag;
        $article->tags()->save($tag);
        return redirect()->route('articles.index')->with('success', 'Pomyślnie utworzono post');
    }

    public function edit($id)
    {
        $article = $this->repository->findById($id);
        $categories = Category::all();
        return view('dashboard.smigiela.blog.articles.article-edit', compact('article', 'categories'));
    }

    public function update(ArticleUpdate $request, $id)
    {
        $article = $this->repository->update($id, $request->validated());
        return redirect()->back()->with('success', 'Pomyślnie zaktualizowano post');
    }

    public function delete($id)
    {
        $article = $this->repository->remove($id);
        return redirect()->route('articles.index')->with('success', 'Pomyślnie usunięto post');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\smigielapl\Models\Article;
use App\smigielapl\Models\Tag;
use App\smigielapl\Repositories\TagsRepositoryInterface;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    protected $repository;

    public function __construct(TagsRepositoryInterface $repository)
    {
        return $this->repository = $repository;
    }


    public function index()
    {
        $tags = $this->repository->getAll(10);
        return view('dashboard.smigiela.blog.tags.tags-index', compact('tags'));
    }

    public function show($id)
    {
        $tag = Tag::with('articles.taggable')->first();
//        $tag = $this->repository->findById($id);
        return view('dashboard.smigiela.blog.tags.tag-show', compact('tag'));
    }

    public function delete($id)
    {
        //
    }
}

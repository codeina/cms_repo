<?php

namespace App\Http\Controllers\Admin;

use App\smigielapl\Repositories\CategoriesRepositoryInterface;
use App\smigielapl\Validators\CategoryStore;
use App\smigielapl\Validators\CategoryUpdate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    private $repository;

    public function __construct(CategoriesRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $categories = $this->repository->getAll(10);
        return view('dashboard.smigiela.blog.categories.category-index', compact('categories'));
    }

    public function show($id)
    {
        $category = $this->repository->findById($id);
        return view('dashboard.smigiela.blog.categories.category-show', compact('category'));
    }

    public function create()
    {
        return view('dashboard.smigiela.blog.categories.category-create');
    }

    public function store(CategoryStore $request)
    {
        $this->repository->create($request->validated());
        return redirect()->route('categories.index')->with('success', __('Pomyślnie utworzono kategorię'));
    }

    public function edit($id)
    {
        $category = $this->repository->findById($id);
        return view('dashboard.smigiela.blog.categories.category-edit', compact('category'));
    }

    public function update(CategoryUpdate $request, $id)
    {
        $this->repository->update($id, $request->validated());
        return redirect()->route('categories.index')->with('success', __('Pomyślnie zaktualizowano kategorię'));
    }

    public function delete($id)
    {
        $this->repository->remove($id);
        return redirect()->route('categories.index')->with('success', __('Pomyślnie usunięto kategorię'));
    }
}

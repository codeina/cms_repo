<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryInterface
{
    /**
     * Return paginate records: limit is number objects per page.
     * @param $limit
     */
    public function getAll($limit);

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model;

    /**
     * @param $id
     * @return Model
     */
    public function findById($id): ?Model;

    /**
     * @param $slug
     * @return Model
     */
    public function findBySlug($slug): Model;

    /**
     * @param $relation
     * @return Model
     */
    public function getWithRelation($relation): Model;

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data): bool;

    /**
     * @param $id
     * @return mixed
     */
    public function remove($id);
}

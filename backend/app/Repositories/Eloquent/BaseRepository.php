<?php

namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAll($limit)
    {
        return $this->model->paginate($limit);
    }

    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    public function findById($id): ?Model
    {
        return $this->model->findOrFail($id);
    }

    public function update($id, $data): bool
    {
        return $this->model->find($id)->update($data);
    }
    public function remove($id)
    {
        return $this->findById($id)->delete();
    }

    public function findBySlug($slug): Model
    {
       return $this->where('slug', $slug)->firstOrFail();
    }

    public function getWithRelation($relation): Model
    {
        return $this->with($relation);
    }
}
